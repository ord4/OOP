// (c) 2017 Orion Davis
// All rights reserved

#include "game.hpp"

#include <iostream>         

void WarGame::prep(Deck& d) {
    // Shuffle deck
    d.shuffle();
    
    // Dist the cards to the players
    for (int i = 0; i < d.d.size() / 2; ++i) {
        //std::cout << d.d[i] << ' ';
        p1.add_to_pile(d.d[i]);
    }

    //std::cout << '\n' << '\n';  

    for(int i = 26; i < d.size(); ++i) {
        //std::cout << d.d[i] << ' ';
        p2.add_to_pile(d.d[i]);
    }
    //std::cout << '\n';
}   

void WarGame::run() {
    bool game_flag = true;
    while (game_flag) {
        // Check that both players still have cards
        if (p1.empty_pile() || p2.empty_pile()) {
            //std::cout << "Someone is out of cards.\n";
            game_flag = false;
        }
        // Both players have cards, we can continue
        else {
            // Draw cards
            Card p1_c = p1.draw_top();
            Card p2_c = p2.draw_top();

            // Compare
            if (p1_c.get_rank() < p2_c.get_rank()) {
                //std::cout << "Hand won, p2.\n";

                // Player adds the cards right after their victory to their hand
                // TODO: add to pile in a random order
                p2.add_to_pile(p2_c);
                p2.add_to_pile(p1_c);
            }
            else if (p2_c.get_rank() < p1_c.get_rank()) {
                //std::cout << "Hand won, p1.\n";

                // Player adds the cards right after their victory to their hand
                // TODO: add to pile in a random order
                p1.add_to_pile(p1_c);
                p1.add_to_pile(p2_c);
            }
            else {
                //std::cout << "WAR!\n";
                go_to_war();
            }
        }
    }
    if (p1.empty_pile()) {
        std::cout << "P2 Wins!! (:\n";
    }
    else if (p2.empty_pile()) {
        std::cout << "P1 Wins!! :)\n";
    }
}

void WarGame::go_to_war() {
    // Draw 3 cards each, if both can
    if (p1.cards_in_pile() >= 4 && p2.cards_in_pile() >= 4) {
        //std::cout << "The War is beginning...\n";

        Card p1_s1 = p1.draw_top(); 
        Card p1_s2 = p1.draw_top(); 
        Card p1_s3 = p1.draw_top(); 
                    
        Card p2_s1 = p2.draw_top(); 
        Card p2_s2 = p2.draw_top(); 
        Card p2_s3 = p2.draw_top(); 

        Card p1_c2 = p1.draw_top();
        Card p2_c2 = p2.draw_top();
                    
        if (p1_c2.get_rank() < p2_c2.get_rank()) {
            //std::cout << "Hand won, p2.\n";

            // Player adds the cards right after their victory to their hand
            // TODO: add to pile in a random order
            p2.add_to_pile(p2_c2);
            p2.add_to_pile(p2_s3); 
            p2.add_to_pile(p2_s2); 
            p2.add_to_pile(p2_s1); 
            // How to handle the initial card that was played to trigger the war?
            //p2.add_to_pile(p2_c);
                        
            p2.add_to_pile(p1_c2);
            p2.add_to_pile(p1_s3); 
            p2.add_to_pile(p1_s2); 
            p2.add_to_pile(p1_s1); 
            //p2.add_to_pile(p1_c);
        }
        else if (p2_c2.get_rank() < p1_c2.get_rank()) {
            //std::cout << "Hand won, p1.\n";

            // Player adds the cards right after their victory to their hand
            // TODO: add to pile in a random order
            p1.add_to_pile(p1_c2);
            p1.add_to_pile(p1_s3); 
            p1.add_to_pile(p1_s2); 
            p1.add_to_pile(p1_s1); 
            //p1.add_to_pile(p1_c);
                        
            p1.add_to_pile(p2_c2);
            p1.add_to_pile(p2_s3); 
            p1.add_to_pile(p2_s2); 
            p1.add_to_pile(p2_s1); 
            //p1.add_to_pile(p2_c);
        }
        else {
            //std::cout << "WAR!\n";
            go_to_war();
        }
    }
    else {
        //std::cout << "Someone doesn't have enough cards.\n";
    }
}

