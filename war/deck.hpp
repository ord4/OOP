// (c) 2017 Orion Davis
// All rights reserved

#pragma once

#include "card.hpp"

#include <vector>

class Deck {
private:
//    std::vector<Card> d;

public:
    std::vector<Card> d;
    Deck();
    void shuffle();
    void print();
    int size();
};
