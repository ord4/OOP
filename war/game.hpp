// (c) 2017 Orion Davis
// All rights reserved

#pragma once

#include "card.hpp"
#include "player.hpp"
#include "deck.hpp"

class WarGame {
private:
    //Player p1;
    //Player p2;
    void go_to_war();

public:
    Player p1;
    Player p2;
    WarGame() = default;
    // Setup preps the game and leaves the state for players ready to begin
    void prep(Deck&);

    // execute the game until there is a clear winner
    void run();
};
