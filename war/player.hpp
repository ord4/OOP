// (c) 2017 Orion Davis
// All rights reserved

#pragma once

#include "card.hpp"

#include <queue>

class Player {
private:
    std::queue<Card> pile;

public: 
    void add_to_pile(Card&);
    Card draw_top();
    int cards_in_pile();
    bool empty_pile();
};
