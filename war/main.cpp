// (C) 2017 Orion Davis
// All rights reserved

#include "card.hpp"
#include "deck.hpp"
#include "game.hpp"

#include <iostream>
#include <vector>
#include <random>
#include <algorithm>

int main() {
    for (int i = 0; i < 100; ++i) {
        Deck d;
        WarGame wg;
        wg.prep(d);
        wg.run();

    }

    return 0;
}
