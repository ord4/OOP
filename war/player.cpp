// (c) 2017
// All rights reserved

#include "player.hpp"

#include <iostream>
#include <vector>

void Player::add_to_pile(Card& c) {
    // Can I just push to a private queue? 
    // Does it need to become public?
    this->pile.push(c);
}

Card Player::draw_top() {
    // How can I get the top card into a variable before popping?
    Card c = this->pile.front();
    // Remove the item
    this->pile.pop();
    // Return the drawn card
    return c;
}

int Player::cards_in_pile() {
    return this->pile.size();
}

bool Player::empty_pile() {
    return this->pile.empty();
}
