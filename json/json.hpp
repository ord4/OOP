// (c) 2017 Orion Davis
// All rights reserved

#include <iostream>
#include <string>
#include <vector>
#include <iterator>

struct Value {
    // No unique properties?
    // Define a virtual print function
    virtual void print() = 0;
};

// Below are all the definitions for the different representations
// of a value

struct String : Value, std::string {
    String(std::string str) {
        s = str;
    }
    std::string s;
    void print() override {
        std::cout << s << '\n';
    }
};

struct Number : Value {
    Number(float x) {
        n = x;
    }
    float n;
    void print() override {
        std::cout << n;
    }
};

// A collection of key-value pairs
struct Object : Value {
    Object() = default;
    Object(std::string str, Value *val) {
        key = str;
        value = val;
    }
    // Has any number of key (string) and value (value) pairs
    // Should there be some data structure holding all of the
    // different pairs, or does nested objects suffice?
    std::string key;
    Value *value;

    // To print the object print the key (string) and then use
    // whatever behaivor is defined for the value
    void print() override {
        std::cout << "{ " << key << " : ";
        value->print(); // Getting error: not ignoring void
        std::cout << " }";
    }

};

// Collection of values
struct Array : Value, std::vector<Value*> {
    std::vector<Value*> arr;

    void print() override {
        for(uint i = 0; i < arr.size(); ++i) {
            // use the print function defined for each value
            arr[i]->print();
        }
    }
};

struct Boolean : Value {
    Boolean(bool f) {
        flag = f;
    }
    bool flag;

    void print() override {
        std::cout << (flag ? true : false);
    }
};

// How can this be used to represent an empty field?
struct Null : Value {};
