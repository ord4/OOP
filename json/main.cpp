// (c) 2017 Orion Davis
// All rights reserved

#include "json.hpp"

int main() {
    Object obj;
    obj.key = "first_key";
    obj.value = new Number(3);
    obj.print();

    Object obj2("second_key", new String("this is a another test value!"));
    obj2.print();
    return 0;
}
