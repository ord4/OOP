// (c) 2017 Orion Davis
// All rights reserved

enum Rank {
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King
};

enum Suit {
    Spade,
    Club,
    Heart,
    Diamond
};

enum Color {
    Black, 
    Red
};

struct Card {
    // No properties currently
    
    Card() = default;
};

struct SuitedCard : Card {
    Suit s;
    Rank r;

    SuitedCard(int, int);
};

struct Joker : Card {
    Color c;

    Joker(int);
};
