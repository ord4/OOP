// (c) 2017 Orion Davis
// All rights reserved

#include "deck.hpp"

std::random_device rd;
std::mt19937 g(rd());

Deck::Deck() {
    d.reserve(54);

    for (int s = Spade; s <= Diamond; ++s) {
        for (int r = Ace; r <= King; ++r) {
            SuitedCard sc(s, r);
            d.push_back(sc);
        }
    }

    for (int c = Black; c <= Red; ++c) {
        Joker j(c);
        d.push_back(j);
    }
}

void Deck::shuffle() {
    std::shuffle(d.begin(), d.end(), g);
}
