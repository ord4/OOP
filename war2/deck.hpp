// (c) 2017 Orion Davis
// All rights reserved

#include "card.hpp"

#include <vector>
#include <iterator>
#include <algorithm>
#include <random>

struct Deck {
    std::vector<Card> d;

    Deck();

    void shuffle();
};
