// (c) 2017 Orion Davis
// All rights reserved

#include "card.hpp"

// This constructor expects suit to be in the range 0-3
// and rank to be in the range 0-12
// Otherwise an invalid card will be created
SuitedCard::SuitedCard(int suit, int rank) {
    s = static_cast<Suit>(suit);
    r = static_cast<Rank>(rank);
}

// The given color value should be 0 or 1, otherwise there
// is an invalid color of card
Joker::Joker(int color) {
    c = static_cast<Color>(color);
}
