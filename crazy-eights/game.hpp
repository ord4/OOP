// (c) 2017 Orion Davis
// All rights reserved

#include "deck.hpp"
#include "player.hpp"
#include "pile.hpp"

class CrazyEights {
private:
    // Players
    Player p1;
    Player p2;
    Player p3;
    Player p4;

    // Deck
    Deck d;

    // Piles
    Pile draw;
    Pile discard;
     

public:
    void setup_game();
    void game_loop();
    void take_turn(Player&);
};
