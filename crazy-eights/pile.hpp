// (c) 2017 Orion Davis
// All rights reserved

#include "card.hpp"

#include <stack>

class Pile {
private:
    std::stack<Card> p;

public:
    Card draw();
    void add(Card&);
};
