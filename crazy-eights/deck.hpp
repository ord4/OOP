// (c) 2017 Orion Davis
// All rights reserved

#pragma once

#include "card.hpp"
#include "player.hpp"

#include <vector>

class Deck {
private:
    std::vector<Card> d;

public:
    Deck();
    void shuffle();
    void print();
    int size();
    void deal_card(Player&);
};
