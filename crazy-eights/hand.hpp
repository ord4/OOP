// (c) 2017 Orion Davis
// All rights reserved

#include "card.hpp"

#include <vector>

class Hand {
private:
    std::vector<Card> h;

public:
    void print_hand();
    void add_card(Card&);

    // Card to be removed will be based on some sort of onput from the user on what card they want to use
    // How should the user input the card they want to play?
    // Should it just be like they are output and run through a big switch to craft the actual card?
    void remove_card(Card&);

    
};
