// (c) 2017 Orion Davis
// All rights reserved

#include "deck.hpp"
#include "card.hpp"

#include <algorithm>
#include <random>
#include <iterator>
#include <iostream>

std::random_device rd;
std::mt19937 g(rd());

Deck::Deck() {
    d.reserve(52);

    for (int s = Spade; s <= Heart; ++s) {
        for (int r = Ace; r <= King; ++r) {
            Card c(r, s);
            d.push_back(c);
        }
    }
}

void Deck::shuffle() {
    std::shuffle(d.begin(), d.end(), g);
}

void Deck::print() {
    for(Card c : d) {
        std::cout << c << ' ';
    }
    std::cout << '\n';
}

int Deck::size() {
    return d.size();
}

void Deck::deal_card(Player& p) {
    p.add_card(d.front());
    d.erase(front());
}
