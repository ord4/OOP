// (c) 2017 Orion Davis
// All rights reserved

#include "pile.hpp"

Card Pile::draw() {
    Card c = p.top();
    p.pop();
    return c;
}

void Pile::add(Card& c) {
    p.push(c);
}
