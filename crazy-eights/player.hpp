// (c) 2017 Orion Davis
// All rights reserved

#include "card.hpp"
#include "deck.hpp"
#include "hand.hpp"

class Player {
private:
    // Every player has a hand 
    Hand h;  

public:
    // Functions needed:
    // - Pick up card
    // - Play card
    // - Select card?
    
    // Take the top card from the draw pile and add it to the player's hand
    // But will they have to know about the pile and all that too?
    // Return something that can be plugged right into play_card()?
    void draw_card();

    // Input from the user on which card to play from those printed out when they start their turn
    void select_card();

    // Based on the input of select card
    // Return with the card being played?
    // Going to become the new top of the playing pile
    Card play_card();

    void display_hand();

    bool full_hand();
};
