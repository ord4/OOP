// (c) 2017 Orion Davis
// All rights reserved

#include "game.hpp"

void CrazyEights::setup_game() {
    // Shuffle the deck
    d.shuffle();

    // Deal cards to the payers
    // Only deal eights cards to the players
    // Should there be a pre-condition with deal cards?
    for (int i = 0; i < 8; ++i) {
        d.deal_card(p1);
        d.deal_card(p2);
        d.deal_card(p3);
        d.deal_card(p4);
    }

    // Push remaining cards into the draw pile
    for (int i = 0; i < d.d.size(); ++i) {
        draw.add(d.d[i]);
        d.d.erase(i);
    }

    // Draw the top card to start the discard pile
    discard.add(draw.draw());
}

void CrazyEights::game_loop() {
    // While everyone has cards
    // Get a helper function for the while loop condition
    while (!(p1.h.v.size == 0 || p2.h.v.size == 0 || p3.h.v.size == 0 || p4.h.v.size == 0)) {
        take_turn(p1);
        take_turn(p2);
        take_turn(p3);
        take_turn(p4);

        // Should be checking before everyone's turn if someone is out of cards
    }

        // Player 1 turn
        // Player 2 ... etc.
}

void CrazyEights::take_turn(Player& p) {
    // Diplay the top card of discard
    std::cout << "Top Discard: " << discard.p.top() << '\n';

    // Display the user's hand
    p.display_hand();

    // User inputs the card they want to play, or d/draw to draw a card
    // Then card is played
    Card c = p.play_card(p.select_card());
    discard.add(c);

    // Turn is over?
}
