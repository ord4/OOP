### Hand
* Collection of cards
* Add a card
* Remove 
* Print out entire hand
* No size limitation

### Player
* Has a hand
* Play a card from their hand
    * Some input so the user can choose the right card to play
    * In the act of playing a card it needs to be removed from their hand
* Draw a card if they have nothing to play
    * If they have no card they can play they should be able to enter something to say they need to draw
    * If the new card cannot be played immediatley, add it to their add
    * Else, the new card will then be played and become the new top of the discard

### Discard
* Could be implemented as a stack
* Can be popped and added back into a drawing pile when it is full/pile is empty

### Draw Pile
* Implemented as queue?
* Fill the queue from a shuffled vector from discard when out of cards

### Misc Notes
* Don't include valid card play for now, leave it up to the user to play a correct card (This is bad in the end!)
