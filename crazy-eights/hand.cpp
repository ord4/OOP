// (c) 2017 Orion Davis
// All rights reserved

#include "hand.hpp"

#include <iostream> 

void Hand::print_hand() {
    for (Card c : h) {
        std::cout << c << ' ';
    }
    std::cout << '\n';
}

void Hand::add_card(Card& c) {
    h.push_back(c);
}

/*
void Hand::remove_card(Card& c) {
    // User will input in some way the card they are playing and then that card will try to be removed from their hand
    for (Card crd : h) {
        if (crd == c) {
            // Card object will already be living based on the input of the user
            // Should indicate whether or not the card was actually removed
            h.erase(crd);
        }
    }
}
*/
